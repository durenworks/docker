#!/bin/bash

RUN_FILE=/var/run/setup_laravel.run

echo 'Waiting for setup_laravel to finish'
while [ -e "$RUN_FILE" ]; do
    sleep 1
done

echo 'Start unit test'
vendor/bin/phpunit
