#!/bin/bash
# Laravel 4 initialization script

RUN_FILE=/var/run/setup_laravel.run
MYSQL_RUN_FILE=/var/run/setup_mysql.run

function setup_laravel()
{
	# Redirect to syslog
	while ! logger -t $(basename $0); do
		sleep 1
	done

	exec 1> >(logger -t $(basename $0)) 2>&1

	echo 'Clear up storage folder'
	find app/storage -type f -not -name "\.*" -delete
	
	echo 'Chmod storage folder'
	chmod a+w -R app/storage

	echo 'Waiting for mysqld to come online'
	while [ -e "$MYSQL_RUN_FILE" ] || ! mysqladmin ping --silent; do
	    sleep 1
	done

	echo 'Drop database '"$MYSQL_DB"
	mysql -uroot -p"$MYSQL_PASSWORD" -e "DROP DATABASE IF EXISTS $MYSQL_DB"

	echo 'Create database '"$MYSQL_DB"
	mysql -uroot -p"$MYSQL_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS $MYSQL_DB"

	echo 'Start composer installation'
	php /build/script/composer.phar install --dev
	
	if [ -n "${LARAVEL_MIGRATE:+1}" ]; then
		if [ -n "${LARAVEL_MIGRATE_SEED:+1}" ]; then
			echo 'Start migration with seeding'
			php artisan migrate --seed
		else
			echo 'Start migration'
			php artisan migrate
		fi
	fi

	echo 'Setup development enviroment done'

	rm -f "$RUN_FILE"
}

touch "$RUN_FILE" && setup_laravel &
