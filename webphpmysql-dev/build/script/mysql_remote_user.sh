#!/bin/bash
# mysql_remote_user.sh
# Enable root user to be accessed from remote host

# Start mysql if no instance found
MYSQL_INSTANCE=1
if ! mysqladmin ping --silent; then
	MYSQL_INSTANCE=0
	echo 'Starting mysqld'
	mysqld_safe &
fi

echo 'Waiting for mysqld to come online'
while ! mysqladmin ping --silent; do
	sleep 1
done

echo 'Setting up remote user'
mysql -uroot -p"$MYSQL_PASSWORD" -e "CREATE USER 'root'@'%' IDENTIFIED BY '$MYSQL_PASSWORD'; GRANT ALL ON *.* TO 'root'@'%';"

if [ "$MYSQL_INSTANCE" -eq 0 ]; then
	echo 'Shutting down mysqld'
	mysqladmin -uroot -p"$MYSQL_PASSWORD" shutdown
fi

echo 'Setup complete'
