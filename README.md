# Durenworks Dockerfile #

This repository is collection of Dockerfile that used inside Durenworks to help developers create consistent dev system.

### What inside this repository? ###

#### webphp-dev ####
Contains combination of nginx and php in one docker container.

#### webphpmysql-dev ####
Contains nginx, php and mysql to run php based web project faster and easier.

#### laravelmysql-dev ####
Contains nginx, php, mysql, and composer complete with simple bash script that initialize laravel project.

### Contact ###

If you found issues, problem, suggestion and comments about this Dockerfile, you can contact our engineer, Airlangga Cahya Utama <airlangga (at) durenworks (dot) com>.