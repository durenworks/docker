#!/bin/bash
# Laravel 4 initialization script

RUN_FILE=/var/run/setup_laravel.run

touch "$RUN_FILE"

# Sleep 1 sec to wait syslog ready
sleep 1

# Redirect to syslog
exec 1> >(logger -t "$(basename $0)") 2>&1

echo 'Clear up storage folder'
find app/storage -type f -not -name "\.*" -delete

echo 'Chmod storage folder'
chmod a+w -R app/storage

# Run spesific db driver
if [ -n "${LARAVEL_DB_DRIVER:+1}" ]; then
	case "${LARAVEL_DB_DRIVER^^}" in
		MYSQL)
			echo 'Waiting for mysqld to come online'
			while ! mysqladmin ping -hdb --silent; do
			    sleep 1
			done

			echo 'Drop database '"$LARAVEL_DB_NAME"
			mysql -hdb -uroot -p"$LARAVEL_DB_PASSWORD" -e "DROP DATABASE IF EXISTS $LARAVEL_DB_NAME"

			echo 'Create database '"$LARAVEL_DB_NAME"
			mysql -hdb -uroot -p"$LARAVEL_DB_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS $LARAVEL_DB_NAME"
			;;
		*)
			echo 'Unknow Database Driver '"$LARAVEL_DB_DRIVER"
			;;
	esac
fi

echo 'Start composer installation'
php /build/script/composer.phar install --dev

if [ -n "${LARAVEL_MIGRATE:+1}" ]; then
	if [ -n "${LARAVEL_MIGRATE_SEED:+1}" ]; then
		echo 'Start migration with seeding'
		php artisan migrate --seed
	else
		echo 'Start migration'
		php artisan migrate
	fi
fi

echo 'Setup development enviroment done'

rm -f "$RUN_FILE"
