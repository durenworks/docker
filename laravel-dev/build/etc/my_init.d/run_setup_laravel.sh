#!/bin/bash
# Laravel 4 initialization script start

/build/script/setup_laravel.sh &

if [ -n "${DOCKER_BEANSTALKD:+1}" ]; then
	echo "Enabling beanstalkd"
else
	echo "Disabling beanstalkd"
	touch /etc/service/beanstalkd/down
fi
