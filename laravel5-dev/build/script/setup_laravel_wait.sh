#!/bin/bash
# Wait for laravel 5 setup done

DONE_FILE=/var/run/setup_laravel.done

# Delay 2 secs to make sure setup_laravel launched correctly
sleep 2

while [ ! -e "$DONE_FILE" ]; do
    sleep 1
done
