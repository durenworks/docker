#!/bin/bash
# Laravel 5 initialization script

DONE_FILE=/var/run/setup_laravel.done

rm -f "$DONE_FILE"

# Enable bash glob extension
shopt -s extglob

# Sleep 1 sec to wait syslog ready
sleep 1

# Redirect to syslog
exec 1> >(logger -t "$(basename $0)") 2>&1

# App source is defined
# Copy it to our web app patch
if [ -n "${APP_SOURCE:+1}" ]; then
	echo 'Copy web source code'
	cp -R "$APP_SOURCE/".!(|.|git) "$APP_SOURCE/"* .
fi

# Clear up storage
echo 'Clear up storage folder' && \
find storage -type f -not -name "\.*" -delete && \
echo 'Chmod storage folder' && \
chmod a+rwX -R storage &

# Make sure cache directory writeable
echo 'Chmod cache folder' && \
chmod a+rwX -R bootstrap/cache &

# import env file from env.dev file
# if ENV_FILE specified, force copy it
if [ -n "${ENV_FILE:+1}" ]; then
	echo 'Force set .env file using '"$ENV_FILE"
	rm -f .env
	sed $'s/\r$//' "$ENV_FILE" > .env
	chmod a+rw .env
elif [ ! -e .env ] && [ -e .env.dev ]; then
	echo '.env file not found, using .env.dev'
	sed $'s/\r$//' .env.dev > .env
	chmod a+rw .env
fi

# import enviroment from env file
if [ -e '.env' ]; then
	source '.env'
fi

# check size
NODE_MODULESSIZE=192
if [ -n "${DOCKER_NODE_MODULESSIZE:+1}" ]; then
	NODE_MODULESSIZE="${DOCKER_NODE_MODULESSIZE}"
fi

# check size, remove if smaller
NODE_MODULESSIZE_CURRENT=$(wc -c < .node_modules)
if [ -e .node_modules ] && [ "${NODE_MODULESSIZE_CURRENT}" -lt "${NODE_MODULESSIZE}" ]; then
	echo 'Deleting .node_modules since its size is smaller than required '"${NODE_MODULESSIZE}"'M'
	rm -rf .node_modules
fi

# setup node_modules filesystem
if [ ! -e .node_modules ]; then
	dd if=/dev/zero of=.node_modules bs=1M count="${NODE_MODULESSIZE}"
	mkfs.ext4 -F .node_modules
fi

# check directory
if [ -e node_modules ] && [ ! -d node_modules ]; then
	echo 'Deleting node_modules since it is not directory'
	rm -f node_modules
fi

if [ ! -d node_modules ]; then
	echo 'Create node_modules directory'
	mkdir node_modules
fi

# try to mount
echo 'Mounting node_modules'
mount .node_modules node_modules

# clean up lost+found
rm -rf node_modules/lost+found

# npm installation
echo 'Start npm installation' && \
npm prune && \
npm install && \
echo 'Chmod npm module folder' && \
chmod a+rwX -R node_modules && \
echo 'Run gulp' && \
gulp && \
chmod a+rwX public &

# Run spesific db driver
if [ -n "${DB_DRIVER:+1}" ]; then
	case "${DB_DRIVER^^}" in
		MYSQL)
			echo 'Waiting for mysqld to come online'
			while ! mysqladmin ping -h"$DB_HOST" --silent; do
			    sleep 1
			done

			echo 'Drop database '"$DB_DATABASE"
			mysql -h"$DB_HOST" -uroot -p"$DB_PASSWORD" -e "DROP DATABASE IF EXISTS $DB_DATABASE"

			echo 'Create database '"$DB_DATABASE"
			mysql -h"$DB_HOST" -uroot -p"$DB_PASSWORD" -e "CREATE DATABASE IF NOT EXISTS $DB_DATABASE"
			;;
		*)
			echo 'Database Driver Not Recognized ['"$DB_DRIVER"']'
			;;
	esac
fi

# composer installation and db migration
MIGRATE_COMMAND='echo No Migration'
if [ -n "${DOCKER_MIGRATE:+1}" ]; then
	if [ -n "${DOCKER_MIGRATE_SEED:+1}" ]; then
		echo 'Start migration with seeding'
		MIGRATE_COMMAND='php artisan migrate --seed'
	else
		echo 'Start migration'
		MIGRATE_COMMAND='php artisan migrate'
	fi
fi

echo 'Start composer installation' && \
composer install --prefer-dist && \
$MIGRATE_COMMAND &

wait

# Check for obselete .env file
if [ -e .env ] && [ -e .env.dev ] && [ .env -ot .env.dev ]; then
	echo 'WARNING'
	echo ' .env file is older than .env.dev file'
	echo ' Its possible other developer has change .env.dev file'
	echo ' Save .env or touch it to remove this warning messages'
fi

echo 'Setup Done'

touch "$DONE_FILE"
