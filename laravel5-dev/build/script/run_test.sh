#!/bin/bash

echo 'Waiting for setup_laravel to finish'
/build/script/setup_laravel_wait.sh

echo 'Start unit test'
vendor/bin/phpunit
