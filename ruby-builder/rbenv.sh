#!/bin/bash
#

trap "echo Quit...; exit -1;" SIGINT

USER_ID="$(id -u)"
USER_NAME="$(whoami)"
VERSION=""
SUDO_CMD=()

function display_help {
	echo "simple rbenv docker script"
	echo "usage: $0 -v <version> [ -c <sudo cmd>] <cmd>"
	exit -1
}

while getopts :v:,c: FLAG; do
	case $FLAG in
		v)
			VERSION="$OPTARG"
			;;
		c)
			SUDO_CMD+=("-c" "$OPTARG")
			;;
		*)
			display_help
			;;
	esac
done

if [ -z "$VERSION" ]; then
	display_help
fi

shift $((OPTIND-1))

case "$1" in
	install)
		CMD="rbenv-install"
		shift
		;;
	*)
		CMD="rbenv-run"
		;;
esac

sudo docker run -it \
	-v "$HOME/.rbenv":"$HOME/.rbenv" \
	-v "$HOME/Project":"$HOME/Project" \
	-w "$HOME/.rbenv" \
	ruby-builder run-shim -u"$USER_NAME" -i"$USER_ID" "${SUDO_CMD[@]}" "$CMD" "$VERSION" "$@"
